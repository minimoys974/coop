import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        token : false,
        id: false,
        fullname: false,
        email: false,
        connected: false,
    },
    mutations: {
        setToken(state, token) {
            state.token = token;
        },
        setMembre(state, data) {
            state.id = data._id;
            state.email = data.email;
            state.fullname = data.fullname
        },
        setStatus(state, data) {
            state.connected = data;
        },
        initialiseStore(state) {
            if(localStorage.getItem('store')) {
                this.replaceState(
                    Object.assign(state, JSON.parse(localStorage.getItem('store')))
                );
            }
        }
    },
    actions: {

    }
})
