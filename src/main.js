import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'
import moment from 'moment'

Vue.config.productionTip = false

moment.locale('fr')

window.axios = axios.create({
    baseURL: 'http://coop.api.netlor.fr/api/',
    params : {
        token : false,
    },
    headers: { Authorization: 'Token token=1fe73e4fa0824005911fc678bdac98e8' }
});


store.subscribe((mutation, state) => {
    localStorage.setItem('store', JSON.stringify(state));
});

new Vue({
  router,
  store,
  components: { App },
  template: '<App/>',
  beforeCreate() {
      this.$store.commit('initialiseStore');
  },
  render: h => h(App)
}).$mount('#app')
