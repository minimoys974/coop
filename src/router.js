import Vue from 'vue'
import Router from 'vue-router'
import Connexion from './components/Connexion.vue'
import CreerCompte from './components/CreerCompte.vue'
import Discussions from './components/Discussions.vue'
import CreerDiscussion from './components/CreerDiscussion.vue'
import Discussion from './components/Discussion.vue'
import Membres from './components/Membres.vue'
import Membre from './components/Membre.vue'

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'Connexion',
            component: Connexion
        },
        {
            path: '/CreerCompte',
            name: 'CreerCompte',
            component: CreerCompte
        },
        {
            path: '/Discussions',
            name: 'Discussions',
            component: Discussions,
        },
        {
            path: '/Creerdiscussion',
            name: 'CreerDiscussion',
            component: CreerDiscussion
        },
        {
            path: '/Discussion',
            name: 'Discussion',
            component: Discussion
        },
        {
            path: '/Membres',
            name: 'Membres',
            component: Membres
        },
        {
            path: '/Membre',
            name: 'Membre',
            component: Membre
        }
    ]
})
